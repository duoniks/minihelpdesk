import sqlite3


DATABASE = 'main.db'

sql_create_table_user = """ CREATE TABLE users (
                                    id INTEGER NOT NULL PRIMARY KEY,
                                    name     CHAR (100) NOT NULL,
                                    department CHAR (50) NOT NULL,
                                    password CHAR (32),
                                    is_admin    BOOLEAN NOT NULL DEFAULT False
                            ); """

sql_create_table_chat_message = """CREATE TABLE chat_message (
                                    id      INTEGER PRIMARY KEY NOT NULL,
                                    mess    TEXT NOT NULL,
                                    from_id INTEGER NOT NULL,
                                    to_id   INTEGER NOT NULL,
                                    FOREIGN KEY (from_id) REFERENCES users (id),
                                    FOREIGN KEY (to_id) REFERENCES users (id)
                            );"""

sql_create_table_orders = """ CREATE TABLE orders (
                                    id INTEGER NOT NULL PRIMARY KEY,
                                    user_id INTEGER NOT NULL,
                                    ip_adr CHAR (20) NOT NULL,
                                    request TEXT NOT NULL,
                                    data_time_open DATETIME DEFAULT ((DATETIME('now', 'LocalTime'))),
                                    admin_id INTEGER,
                                    coment_admin TEXT,
                                    data_time_close DATETIME,
                                    FOREIGN KEY (admin_id) REFERENCES users(id),
                                    FOREIGN KEY (user_id) REFERENCES users(id)
                            );"""


conn = sqlite3.connect(DATABASE)
cursor = conn.cursor()


def create_table():
    cursor.execute(sql_create_table_user)
    cursor.execute(sql_create_table_chat_message)
    cursor.execute(sql_create_table_orders)


def insert_user(name, dep, adm, passw):
    sql = """ INSERT INTO users (name, department, is_admin, password) VALUES (:name, :dep, :adm, :pass )"""
    cursor.execute(sql, {'name': name, 'dep': dep, 'adm': adm, 'pass': passw})


def users():
    """Create default user - admin, password - admin"""
    user = 'admin'
    dep = ''
    adm = 1
    passw = '21232f297a57a5a743894a0e4a801fc3'
    insert_user(user, dep, adm, passw)


def get_all_users():
    sql = """ SELECT * FROM users"""
    cursor.execute(sql)
    return cursor.fetchall()


def get_user_id(name):
    sql = (""" SELECT id FROM users WHERE name=:name """, {'name': name})
    cursor.execute(sql[0], sql[1])
    user_id = cursor.fetchone()
    return user_id[0] if user_id else None


def get_order_limit(count):
    sql = """ SELECT
                    orders.id,
                    users.name,
                    orders.data_time_open,
                    orders.request,
                    orders.data_time_close,
                    orders.coment_admin,
                    orders.ip_adr
                FROM orders, users
                WHERE users.id=orders.user_id
                ORDER BY orders.id DESC
                LIMIT :count"""
    cursor.execute(sql, {'count': count})
    res = cursor.fetchall()
    print(res)
    return res


get_order_limit(5)
#create_table()
#users()



conn.commit()
cursor.close()
conn.close()
print('---well done---')
