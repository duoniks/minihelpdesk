import sqlite3
import hashlib
from datetime import datetime
from flask import Flask, render_template, request, jsonify, session, redirect, url_for


app = Flask(__name__)
data_min = '2019:01:01'
data_max = '2019:01:02'

def get_user_by_name(name):
    return ("""SELECT * FROM users WHERE name=:name """, {'name': name})


def get_user_id_by_name(name):
    return ("""SELECT id FROM users WHERE name=:name """, {'name': name})


def get_all_admin():
    return ("""SELECT * FROM users WHERE is_admin=1 """,)


def get_all_users():
    return ("""SELECT * FROM users""",)


def get_user_by_id(id):
    return ("""SELECT * FROM users WHERE id=:id """, {'id': id})


def get_user_by_login_pass(login, passw):
    return ("""SELECT * FROM users WHERE name=:login AND password=:passw""", {'login': login, 'passw': passw})


def create_user(name, dep, password, is_admin):
    return (""" INSERT INTO users(
                        name,
                        department,
                        password,
                        is_admin)
                     VALUES (:name, :dep, :pass, :adm)""",
            {
                'name': name,
                'dep': dep,
                'pass': password,
                'adm': is_admin
            })


def update_user_sql(id, name, dep, password, is_admin):
    return (""" UPDATE users
                SET
                    name = :name,
                    department = :dep,
                    password = :pass,
                    is_admin = :adm
                WHERE id = :id""",
            {
                'id': id,
                'name': name,
                'dep': dep,
                'pass': password,
                'adm': is_admin
            })


def create_order(user_id, req, ip_adr):
    return (""" INSERT INTO orders(
                    user_id,
                    ip_adr,
                    request,
                    data_time_open)
                 VALUES (:user_id, :ip_adr, :request, DATETIME('now', 'localtime'))""",
            {
                'user_id': user_id,
                'ip_adr': ip_adr,
                'request': req
            })


def close_order(id_admin, coment, order_id):
    return (""" UPDATE orders SET
                    admin_id=:id_admin,
                    coment_admin=:coment,
                    data_time_close=DATETIME('now', 'localtime')
                WHERE id = :id""",
            {
                'id_admin': id_admin,
                'coment': coment,
                'id': order_id
            })


def get_all_orders(count=None):
    sql = """SELECT
                    orders.id,
                    users.name,
                    orders.data_time_open,
                    orders.request,
                    orders.data_time_close,
                    orders.coment_admin,
                    orders.ip_adr
                FROM orders, users
                WHERE users.id=orders.user_id
                ORDER BY orders.id DESC
            """
    sql2 = """SELECT
                    orders.id,
                    users.name,
                    orders.data_time_open,
                    orders.request,
                    orders.data_time_close,
                    orders.coment_admin,
                    orders.ip_adr
                FROM orders, users
                WHERE users.id=orders.user_id
                ORDER BY orders.id DESC
                LIMIT :count"""

    if count:
        return sql2, {'count': count},
    else:
        return sql,


def get_last_orders():
    return ("""SELECT
                        orders.id,
                        users.name,
                        orders.data_time_open,
                        orders.request,
                        orders.data_time_close,
                        orders.coment_admin,
                        orders.ip_adr
                    FROM orders, users
                    WHERE orders.id = (SELECT MAX(id) FROM orders) AND users.id=orders.user_id
                """,)


def get_order_by_data(data_start, data_end):
    """format data 'GGGG-MM-DD HH:MM:SS'"""
    return (""" SELECT  orders.id,
                        users.name,
                        orders.data_time_open,
                        orders.request,
                        orders.data_time_close,
                        orders.coment_admin,
                        orders.ip_adr
                FROM orders, users
                WHERE users.id=orders.user_id AND orders.data_time_open
                BETWEEN :data_start AND :data_end;
            """, {'data_start': data_start, 'data_end': data_end})


def get_order(id):
    return ("""SELECT
                    orders.id,
                    users.name,
                    orders.data_time_open,
                    orders.request,
                    orders.data_time_close,
                    orders.coment_admin,
                    orders.ip_adr
                FROM orders, users
                WHERE users.id=orders.user_id AND orders.id=:id
            """, {'id': id})


def connect_db(data):
    conn = sqlite3.connect('main.db')
    cursor = conn.cursor()
    if data:
        if len(data) > 1:
            #print(data)
            cursor.execute(data[0], data[1])
            _result = cursor.fetchall()
            last_id = cursor.lastrowid
        else:
            cursor.execute(data[0])
            _result = cursor.fetchall()
            last_id = cursor.lastrowid


    #cursor.execute(""" SELECT name FROM sqlite_master;""")
    #print(last_id)
    #print(cursor.fetchall(), result)

    conn.commit()
    cursor.close()
    conn.close()
    return {'res': _result, 'id': last_id}


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/', methods=['GET', 'POST'])
def index():
    user = None
    dep = None
    if len(session) != 0:
        user = session['user']
        dep = session['dep']
        render_template('index.html', user=user, dep=dep)
    return render_template('index.html', user=user, dep=dep)


@app.route('/send', methods=['GET', 'POST'])
def send():
    if request.method == 'POST':
        name = request.form.get('name')
        dep = request.form.get('dep')
        mess = request.form.get('message')
        session['user'] = name
        session['dep'] = dep
        user_id = connect_db(get_user_id_by_name(name))['res']
        if user_id:
            _id = user_id[0][0]
        else:
            connect_db(create_user(name, dep, '', 0))
            _req = connect_db(get_user_id_by_name(name))
            user_id = _req['res']
            #print(_req['id'])
            _id = user_id[0][0]
        session['id'] = _id
        last_id = connect_db(create_order(_id, mess, request.remote_addr))['id']
    return render_template('send.html', ticket_id=last_id)


@app.route('/update_time')
def update_time():
    return jsonify({'time': datetime.now().strftime('%Y-%m-%d %H:%M:%S')})


@app.route('/update_ticket')
def update_ticket():
    orders = connect_db(get_last_orders())['res']
    if not orders:
        return jsonify(None)
    else:
        return jsonify({'ord': orders[-1][0],
                    'tr': '''<tr id="red">
            <td id="id-'''+str(orders[-1][0])+'''" width="20%" data="'''+str(orders[-1][0])+'''">
              <div id="time_op" align="center">Время подачи заявка<br />'''+str(orders[-1][2])+'''</div>
                <div>id-'''+str(orders[-1][0])+'''</div>
                <div align="center">Время закрытия заявка<br /></div>
            </td>
            <td >
                <div id="user" align="center" >'''+str(orders[-1][1])+'''</div>
                <div id="masseg" >'''+str(orders[-1][3])+'''</div>
                <hr />
                <div id="coment"></div>
                <div id="ip" align="center">'''+str(orders[-1][6])+'''<input hidden="true" type="button" class="connect" value="Connect"></div>
            </td>
            <td width="5%">
                <a href="/order'''+str(orders[-1][0])+'''"><input type="button" id="open" value="Open"></a>
            </td>
        </tr>'''})


@app.route('/history_data', methods=['POST'])
def history_data():
    """format data 'GGGG-MM-DD HH:MM:SS'"""
    if request.method == 'POST':

        sd = request.form.get('data_start')
        ed = request.form.get('data_end')
        if len(session) != 0:
            user = connect_db(get_user_by_name(session['user']))['res']
            if user[0][4] != 1:
                return render_template('admin_login.html')
            session['id'] = user[0][0]
            session['user'] = user[0][1]
            session['dep'] = user[0][2]
            session['password'] = user[0][3]
            session['is_admin'] = user[0][4]
            orders = connect_db(get_order_by_data(sd+' 00:00:00', ed+' 23:59:59'))['res']
            if len(orders)>0:
                _ord = connect_db(get_all_orders())['res']
                _temp = [i[2] for i in _ord]
                data_min = min(_temp).split(' ')[0]
                data_max = max(_temp).split(' ')[0]
                return render_template('admin.html', user=session['user'], orders=orders, data_min=data_min, data_max=data_max)
            else:
                return render_template('admin.html', user=session['user'], orders=None, data_min=data_min, data_max=data_max)

    return render_template('admin_login.html')

@app.route('/history', methods=['GET'])
def history():
    if request.method == 'GET':
        if len(session) != 0:
            user = connect_db(get_user_by_name(session['user']))['res']
            if user[0][4] != 1:
                return render_template('admin_login.html')
            session['id'] = user[0][0]
            session['user'] = user[0][1]
            session['dep'] = user[0][2]
            session['password'] = user[0][3]
            session['is_admin'] = user[0][4]

            orders = connect_db(get_all_orders())['res']
            if len(orders)>0:
                _ord = connect_db(get_all_orders())['res']
                _temp = [i[2] for i in _ord]
                data_min = min(_temp).split(' ')[0]
                data_max = max(_temp).split(' ')[0]
                return render_template('admin.html', user=session['user'], orders=orders, data_min=data_min, data_max=data_max)
            else:
                return render_template('admin.html', user=session['user'], orders=None, data_min=data_min, data_max=data_max)

    return render_template('admin_login.html')


@app.route('/admin-<count>', methods=['GET'])
@app.route('/admin', methods=['GET', 'POST'])
def admin(count=10):
    if request.method == 'POST':
        form = request.form
        login = form['user']
        passw = hashlib.md5(form['pass'].encode())
        admin = connect_db(get_user_by_login_pass(login, passw.hexdigest()))['res']

        if len(admin) != 0:
            session['id'] = admin[0][0]
            session['user'] = login
            session['dep'] = admin[0][2]
            session['password'] = admin[0][3]
            session['is_admin'] = admin[0][4]

            orders = connect_db(get_all_orders(count))['res']
            if len(orders)>0:
                _ord = connect_db(get_all_orders())['res']
                _temp = [i[2] for i in _ord]
                data_min = min(_temp).split(' ')[0]
                data_max = max(_temp).split(' ')[0]
                return render_template('admin.html', user=session['user'], orders=orders, data_min=data_min, data_max=data_max)
            else:
                return render_template('admin.html', user=session['user'], orders=None, data_min=data_min, data_max=data_max)

    elif request.method == 'GET':
        if len(session) != 0:
            user = connect_db(get_user_by_name(session['user']))['res']
            if user[0][4] != 1:
                return render_template('admin_login.html')
            session['id'] = user[0][0]
            session['user'] = user[0][1]
            session['dep'] = user[0][2]
            session['password'] = user[0][3]
            session['is_admin'] = user[0][4]

            orders = connect_db(get_all_orders(count))['res']
            if len(orders)>0:
                _ord = connect_db(get_all_orders())['res']
                _temp = [i[2] for i in _ord]
                data_min = min(_temp).split(' ')[0]
                data_max = max(_temp).split(' ')[0]
                return render_template('admin.html', user=session['user'], orders=orders, data_min=data_min, data_max=data_max)
            else:
                return render_template('admin.html', user=session['user'], orders=None, data_min=data_min, data_max=data_max)

    return render_template('admin_login.html')


@app.route('/order<id>', methods=['GET', 'POST'])
def order(id):
    if len(session) == 0:
        return render_template('404.html'), 404
    else:
        user = connect_db(get_user_by_name(session['user']))['res'].pop()
        if user[4]!=1:
            return render_template('404.html'), 404
    if request.method == 'POST':
        coment = request.form['coment']
        connect_db(close_order('1', coment, str(id)))
        return redirect(url_for('admin'))
    order = connect_db(get_order(str(id)))['res']
    return render_template('order.html', id=id, order=order)


@app.route('/order-<id>')
def show(id):
    order = connect_db(get_order(id))['res']
    return render_template('show.html', id=id, order=order)


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('admin'))


@app.route('/all_users')
def all_users():
    if len(session) == 0:
        return render_template('404.html'), 404
    else:
        user = connect_db(get_user_by_name(session['user']))['res'].pop()
        if user[4]!=1:
            return render_template('404.html'), 404
    users = connect_db(get_all_users())['res']
    return render_template('users.html', users=users)


@app.route('/user<id>', methods=['GET', 'POST'])
def user(id):
    if len(session) == 0:
        return render_template('404.html'), 404
    else:
        _user = connect_db(get_user_by_name(session['user']))['res'].pop()
        if _user[4]!=1:
            return render_template('404.html'), 404
    user = None
    if id != '0':
        user = connect_db(get_user_by_id(str(id)))['res'].pop()
    return render_template('/user.html', id=id, user=user)


@app.route('/update_user<id>', methods=['POST'])
def update_user(id):
    if len(session) == 0:
        return render_template('404.html'), 404
    else:
        user = connect_db(get_user_by_name(session['user']))['res'].pop()
        if user[4]!=1:
            return render_template('404.html'), 404
    if request.method == 'POST':
        name = request.form.get('name')
        dep = request.form.get('dep')
        password = hashlib.md5(request.form.get('password').encode())
        is_admin = request.form.get('is_admin')# return on or None

        if id=='0':
            connect_db(create_user(name, dep, password.hexdigest(), 1 if is_admin=='on' else 0))
            #print(name, dep, password.hexdigest(), 1 if is_admin=='on' else 0)
        else:
            connect_db(update_user_sql(id, name, dep, password.hexdigest(), 1 if is_admin=='on' else 0))
            #print(id, name, dep, password.hexdigest(), 1 if is_admin=='on' else 0)
    return redirect(url_for('all_users'))

def main():
    app.secret_key = 'FH&thk38gT6_[dv04F*iFks2#@2-d[b^'
    app.run(host='0.0.0.0', port='3596')


if __name__ == '__main__':
    app.secret_key = 'FH&thk38gT6_[dv04F*iFks2#@2-d[b^'
    app.run(host='127.0.0.1', port='3000')
